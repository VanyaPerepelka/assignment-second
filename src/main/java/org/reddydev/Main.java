package org.reddydev;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    public static void main(String[] args) {
        logger.info("Hello world");
        logger.info("This was vi-ed and pushed on ec2 instance ");
    }
}
